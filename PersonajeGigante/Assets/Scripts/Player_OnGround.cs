using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_OnGround : MonoBehaviour
{
    public RagdollPlayer_Controller playerController;
    void Start()
    {
        playerController = GameObject.FindObjectOfType<RagdollPlayer_Controller>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        playerController.onGround = true;
    }
}
