using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollPlayer_Controller : MonoBehaviour
{
    public float speed;
    public float strafeSpeed;
    public float jumpForce;
    public bool onGround;

    public Rigidbody rbHips;
    void Start()
    {
        rbHips = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.W))
        {
            if(Input.GetKey(KeyCode.LeftShift))
            {
                rbHips.AddForce(transform.right * speed * 1.5f);
            }
            else 
            {
                rbHips.AddForce(transform.right * speed );
            }
        }
        if (Input.GetKey(KeyCode.S))
        {
            rbHips.AddForce(-transform.right * speed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            rbHips.AddForce(transform.forward * strafeSpeed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            rbHips.AddForce(-transform.forward * strafeSpeed);
        }

        if(Input.GetAxis("Jump") > 0)
        {
            if(onGround)
            {
                rbHips.AddForce(new Vector3(0, jumpForce, 0));
                onGround = false;
            }
        }
    }
}
